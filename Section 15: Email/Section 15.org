#+TITLE: Section 15
#+SUBTITLE: Email
#+AUTHOR: Mohammed Asir Shahid
#+EMAIL: MohammedShahid@protonmail.com
#+DATE: 2021-08-06

* Sending Emails

This can be a great way to send notifications. For example, if you are doing web scraping and your program finds something of note, perhaps a sale on an item, then it can send you an email notifying you. It can also help you email a bunch of people based on some criteria.

** SMPT - Simple Mail Transfer Protocol

This is the protocol that sending email uses. Python has a module called smptlib which implements this protocol.


#+begin_src python :results output :exports both

import smtplib

conn=smtplib.SMTP("smtp.gmail.com", 587)
print(type(conn))

conn.ehlo()
print(conn.ehlo())

conn.starttls()

conn.login(send_email,password)

conn.sendmail(send_email, recieve_email,"Subject: Test SMPT Email.\n\nDear Asir,\nSo long, and thanks for all the fish.\n\n-Asir")

conn.quit()

#+end_src

#+RESULTS:
: <class 'smtplib.SMTP'>
: (250, b'smtp.gmail.com at your service, [67.84.145.177]\nSIZE 35882577\n8BITMIME\nSTARTTLS\nENHANCEDSTATUSCODES\nPIPELINING\nCHUNKING\nSMTPUTF8')

* Checking Your Email Inbox

We learned how to send emails using SMTP. For checking our received emails, we have a different protocol.

** IMAP - Internet Message Access Protocol

This species how to communicate with your email service provider in order to see your received mail. IMAP is pretty complicated as Email protocols are nearly as old as the internet. Python comes with an impablib module, however we will instead use two third party modules.

#+begin_src bash
pip install imapclient
pip install pyzmail36
#+end_src

#+RESULTS:
| Defaulting  | to      | user       | installation | because | normal                                             | site-packages | is          | not      | writeable |
| Requirement | already | satisfied: | imapclient   | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (2.2.0)       |             |          |           |
| Requirement | already | satisfied: | six          | in      | /usr/lib/python3.9/site-packages                   | (from         | imapclient) | (1.15.0) |           |
| Defaulting  | to      | user       | installation | because | normal                                             | site-packages | is          | not      | writeable |
| Requirement | already | satisfied: | pyzmail36    | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (1.0.4)       |             |          |           |
| Requirement | already | satisfied: | setuptools   | in      | /usr/lib/python3.9/site-packages                   | (from         | pyzmail36)  | (49.1.3) |           |


#+begin_src python :results output :exports both

import imapclient,pyzmail
from datetime import date

conn=imapclient.IMAPClient("imap.gmail.com", ssl=True)

conn.login(email,password)


conn.select_folder("INBOX", readonly=True)

UID=conn.search(["FROM", recieved_from])
UIDs=conn.search(["SINCE" ,date(2021,8,1)])

raw_message=conn.fetch([62266], ["BODY[]", "FLAGS"])

message=pyzmail.PyzMessage.factory(raw_message[62266][b"BODY[]"])

subject=message.get_subject()

print(subject)

sender=message.get_addresses("from")
reciever=message.get_addresses("to")

print(sender, reciever)

print(message.text_part)
print(message.html_part)

print(message.text_part.get_payload().decode("UTF-8"))

#conn.logout

print(conn.list_folders())


#+end_src

#+RESULTS:
#+begin_example
Critical security alert
[('Google', 'no-reply@accounts.google.com')] [('asirshahid1@gmail.com', 'asirshahid1@gmail.com')]
MailPart<*text/plain charset=UTF-8 len=752>
MailPart<*text/html charset=UTF-8 len=4789>
[image: Google]
Access for less secure apps setting has been turned on


asirshahid1@gmail.com
Access for less secure apps setting has been turned on for your Google
Account asirshahid1@gmail.com. If you didn't change it, you should check
what happened.
Check activity
<https://accounts.google.com/AccountChooser?Email=asirshahid1@gmail.com&continue=https://myaccount.google.com/alert/nt/1628292849644?rfn%3D28%26rfnc%3D1%26eid%3D-4292077690557967320%26et%3D0%26anexp%3Dnret-fa>
You can also see security activity at
https://myaccount.google.com/notifications
You received this email to let you know about important changes to your
Google Account and services.
© 2021 Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA

[((b'\\HasNoChildren',), b'/', 'Boomerang'), ((b'\\HasNoChildren',), b'/', 'Boomerang-Returned'), ((b'\\HasNoChildren',), b'/', 'INBOX'), ((b'\\HasNoChildren',), b'/', 'Notes'), ((b'\\HasNoChildren',), b'/', 'Outbox'), ((b'\\HasNoChildren',), b'/', 'Receipts'), ((b'\\HasNoChildren',), b'/', 'Sent'), ((b'\\HasNoChildren',), b'/', 'Trash'), ((b'\\HasChildren', b'\\Noselect'), b'/', '[Gmail]'), ((b'\\All', b'\\HasNoChildren'), b'/', '[Gmail]/All Mail'), ((b'\\Drafts', b'\\HasNoChildren'), b'/', '[Gmail]/Drafts'), ((b'\\HasNoChildren', b'\\Important'), b'/', '[Gmail]/Important'), ((b'\\HasNoChildren', b'\\Sent'), b'/', '[Gmail]/Sent Mail'), ((b'\\HasNoChildren', b'\\Junk'), b'/', '[Gmail]/Spam'), ((b'\\Flagged', b'\\HasNoChildren'), b'/', '[Gmail]/Starred'), ((b'\\HasChildren', b'\\Trash'), b'/', '[Gmail]/Trash'), ((b'\\HasChildren',), b'/', '[Gmail]/Trash/Boomerang-Outbox'), ((b'\\HasNoChildren',), b'/', '[Gmail]/Trash/Boomerang-Outbox/Cancelled'), ((b'\\HasNoChildren',), b'/', '[Gmail]/Trash/SMS'), ((b'\\HasNoChildren',), b'/', '[Gmail]/Trash/Travel'), ((b'\\HasNoChildren',), b'/', '[Gmail]/Trash/Work'), ((b'\\HasNoChildren',), b'/', 'mine')]
#+end_example

Above, we used the search method. When searching, IMAP has a particular syntax that you search with. We can use "ALL", "BEFORE date", "ON date", "SINCE date", "SUBJECT string", "BODY string", "TEXT string", "FROM string", "TO string", "CC string", "SEEN", "UNSEEN", "ANSWERED", "DRAFT", etc.

Above, we also set the inbox to readonly. If we want to be able to delete Emails, we need to set readonly to False. Then we can use the delete_messages function and pass the UIDs that we want to delete.
