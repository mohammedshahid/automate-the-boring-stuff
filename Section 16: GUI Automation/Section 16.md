
# Table of Contents

1.  [Controlling the Mouse from Python](#orgf197a9c)
2.  [Controlling the Keyboard from Python](#orgd22e3ad)



<a id="orgf197a9c"></a>

# Controlling the Mouse from Python

The ultimate automation code tools are programs that control the keyboard and mouse. These programs can send virtual mouse clicks and keystrokes. We will use the third party pyautogui module for this.

    pip install pyautogui

The mouse controlling functions work by XY coordinates that make up your display.

    
    import pyautogui
    
    print(pyautogui.size())
    
    width,height=pyautogui.size()
    
    print(pyautogui.position())
    
    pyautogui.moveTo(3200,1000,duration=1.5)
    
    pyautogui.moveRel(300,-100,duration=1.5)

    
    import pyautogui
    
    pyautogui.click(3534,1500,duration=1)
    pyautogui.middleClick(3534,1500,duration=1)
    
    
    pyautogui.click()

    
    import pyautogui
    
    print(pyautogui.position())
    
    pyautogui.moveTo(2776,645,duration=1)
    
    pyautogui.dragRel(500,0,duration=1)
    pyautogui.dragRel(-500,0,duration=1)
    pyautogui.dragRel(500,0,duration=1)
    pyautogui.dragRel(-500,0,duration=1)
    pyautogui.dragRel(2500,0,duration=1)
    pyautogui.dragRel(-2500,0,duration=1)

PyAutoGui has a failsafe in case you lose control of the mouse. If the mouse goes to the coordinates 0,0, then PyAutoGui will raise an exception and then stop.

We can use the displayMousePosition() function in order to have an easier way to figure out the exact position of any given point. It gives the realtime coordinates and the RGB of the pixel. However, we need to run this from a terminal.


<a id="orgd22e3ad"></a>

# Controlling the Keyboard from Python

Now we will cover the keyboard automation through pyautogui.

    
    import pyautogui
    
    pyautogui.click(1657,1376); pyautogui.typewrite("i Hello World")

What can we do for keys that can&rsquo;t be represented by single string characters, for example the arrow keys, or the shift keys.

    
    import pyautogui
    
    print(pyautogui.position())
    
    #pyautogui.click(); pyautogui.typewrite(['i','a','b','left','right','X','y'])
    
    print(pyautogui.KEYBOARD_KEYS)
    
    
    pyautogui.typewrite(["space",",",'enter','space',','])
    
    pyautogui.press('enter')

If we want to use a keyboard shortcut like control+c, then we can use the hotkey function.

